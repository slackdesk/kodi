# kodi

[Kodi](https://kodi.tv) build scripts for [Slackware Linux](http://www.slackware.com/).

This Kodi on X11 build can be used as a regular application from your menu, or as a standalone environment* (via xinitrc, no other DE's or WM's need to be installed).  

* The standalone environment "technically" doesn't need X but does require some libraries from Slackware's X series packages. Therefore you can run Kodi on X11 as your default environment with xinitrc.kodi, or in standalone "mode" with xinitrc.kodi-standalone. The differences in functionality are currently unknown to me.

## options

Default options are to build on X11, with Kodi's internal ffmpeg and 'Event Clients' turned on.

All build time switches you can throw and their build time defaults can be seen in [core/kodi/README.options](https://gitlab.com/slackdesk/kodi/-/blob/current/core/kodi/README.options).

See [core/kodi/README.composers](https://gitlab.com/slackdesk/kodi/-/blob/current/core/kodi/README.composers) to see what the difference is between X11, Wayland, and GBM builds.

## repo structure

Everything is included in the repo to completely build Kodi.

/core is Kodi's core components (just kodi).  
/deps is dependencies required by Kodi.  

## important

NOTE: My SlackBuilds might be different than SBo. WATCH YOUR DEPENDENCIES! There are some SlackBuilds that I have borrowed from SBo (some of the more complicated ones) so many thanks to the SBo team and the script maintainers!

#

Kodi® is a registered trademark of [XBMC Foundation](https://kodi.wiki/view/XBMC_Foundation)  
Slackware® is a registered trademark of [Patrick Volkerding](http://www.slackware.com/)  
Linux® is a registered trademark of [Linus Torvalds](http://www.linuxmark.org/)

